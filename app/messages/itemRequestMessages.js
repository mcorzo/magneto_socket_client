exports.createItemRequest = function () {
  return {
    trackingId: "trackingId",
    type: "createItemRequest",
    body: {
      "itemId": "fullItemId",
      "tag": "clan_c78c1c0d-01f8-48df-87e0-f8037d92e9b9",
      "requestees": ["irtest2", "irtest3"]
    }
  };
}

exports.createItemRequestContribution = function () {
  return {
    "trackingId": "trackingId",
    "type": "createItemRequestContribution",
    "body": {
      "itemRequestId": "0504f359-714e-4400-9afa-7164377bf5b6"
    }
  }
}
